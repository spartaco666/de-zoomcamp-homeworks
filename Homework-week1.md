## Module 1 Homework

## Docker & SQL

In this homework we'll prepare the environment 
and practice with Docker and SQL


## Question 1. Knowing docker tags

Run the command to get information on Docker 

```docker --help```

Now run the command to get help on the "docker build" command:

```docker build --help```

Do the same for "docker run".

Which tag has the following text? - *Automatically remove the container when it exits* 

- `--rm`


## Question 2. Understanding docker first run 

Run docker with the python:3.9 image in an interactive mode and the entrypoint of bash.
Now check the python modules that are installed ( use ```pip list``` ). 

What is version of the package *wheel* ?

- 0.42.0


## Question 3. Count records 

How many taxi trips were totally made on September 18th 2019?

Tip: started and finished on 2019-09-18. 

Remember that `lpep_pickup_datetime` and `lpep_dropoff_datetime` columns are in the format timestamp (date and hour+min+sec) and not in date.


- 15612

Solution :

``` 
SELECT * FROM public.green_tripdata_raw where 
(lpep_pickup_datetime >= '2019-09-18' and lpep_pickup_datetime < '2019-09-19')
and (lpep_dropoff_datetime >= '2019-09-18' and lpep_dropoff_datetime < '2019-09-19')
```


## Question 4. Largest trip for each day

Which was the pick up day with the largest trip distance
Use the pick up time for your calculations.

- 2019-09-26

Solution : 

 ```
SELECT
	lpep_pickup_datetime,
	trip_distance
FROM public.green_tripdata_raw order by trip_distance desc limit 1
```

- 2019-09-21


## Question 5. The number of passengers

Consider lpep_pickup_datetime in '2019-09-18' and ignoring Borough has Unknown

Which were the 3 pick up Boroughs that had a sum of total_amount superior to 50000?
 
- "Brooklyn" "Manhattan" "Queens"

```
with __source_trip_data as (
	select * from public.green_tripdata_raw
),

__lookup_zones as (
	select * from public.taxi_zones_lookup
),

__join as (
	select
		tp.*,
		lz.location_id as pu_location_id,
		lz.borough as pu_borough,
		lz.zone as pu_zone,
		lz.service_zone as pu_service_zone,
		lz2.location_id as do_location_id,
		lz2.borough as do_borough,
		lz2.zone as do_zone,
		lz2.service_zone as do_service_zone
	from __source_trip_data tp
	left join __lookup_zones lz on tp.pu_location_id = lz.location_id
	left join __lookup_zones lz2 on tp.do_location_id = lz2.location_id 
),

__filter as (
select 
	pu_borough, 
	sum(total_amount) as sum_total_amount
	from __join
	where (lpep_pickup_datetime >= '2019-09-18' and lpep_pickup_datetime < '2019-09-19')
		and pu_borough <> 'Unknown' 
	group by pu_borough
)

select * from __filter where sum_total_amount > 50000

 ```


## Question 6. Largest tip

For the passengers picked up in September 2019 in the zone name Astoria which was the drop off zone that had the largest tip?
We want the name of the zone, not the id.

Note: it's not a typo, it's `tip` , not `trip`

- JFK Airport
```


with __source_trip_data as (
	select * from public.green_tripdata_raw
),

__lookup_zones as (
	select * from public.taxi_zones_lookup
),

__join_pu as (
	select
		tp.*,
		location_id as pu_location_id,
		borough as pu_borough,
		zone as pu_zone,
		service_zone as pu_service_zone
	from __source_trip_data tp
	left join __lookup_zones lz
	on tp.pu_location_id = lz.location_id 
),

__join_do as (
	select
		jp.*,
		location_id as do_location_id,
		borough as do_borough,
		zone as do_zone,
		service_zone as do_service_zone
	from __join_pu jp
	left join __lookup_zones lz
	on jp.do_location_id = lz.location_id 
),

__filter as (
select 
	do_zone,
	max(tip_amount) as max_tip
	from __join_do
	where (lpep_pickup_datetime >= '2019-09-01' and lpep_pickup_datetime < '2019-10-01')
		and pu_zone = 'Astoria'
	group by do_zone
)

select * from __filter order by max_tip desc limit 1
```
- Long Island City/Queens Plaza


## Question 7. Creating Resources

After updating the main.tf and variable.tf files run:

```
terraform apply
```

Output :  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:   + create  Terraform will perform the following actions:    # google_bigquery_dataset.demo_dataset will be created   + resource "google_bigquery_dataset" "demo_dataset" {       + creation_time              = (known after apply)       + dataset_id                 = "demo_dataset"       + default_collation          = (known after apply)       + delete_contents_on_destroy = false       + effective_labels           = (known after apply)       + etag                       = (known after apply)       + id                         = (known after apply)       + is_case_insensitive        = (known after apply)       + last_modified_time         = (known after apply)       + location                   = "EU"       + max_time_travel_hours      = (known after apply)       + project                    = "original-fort-408720"       + self_link                  = (known after apply)       + storage_billing_model      = (known after apply)       + terraform_labels           = (known after apply)     }    # google_storage_bucket.de-zoomcamp-bucket-bucket12414 will be created   + resource "google_storage_bucket" "de-zoomcamp-bucket-bucket12414" {       + effective_labels            = (known after apply)       + force_destroy               = true       + id                          = (known after apply)       + location                    = "EU"       + name                        = "de-zoomcamp-bucket-test1232141"       + project                     = (known after apply)       + public_access_prevention    = (known after apply)       + self_link                   = (known after apply)       + storage_class               = "STANDARD"       + terraform_labels            = (known after apply)       + uniform_bucket_level_access = (known after apply)       + url                         = (known after apply)        + lifecycle_rule {           + action {               + type = "AbortIncompleteMultipartUpload"             }           + condition {               + age                   = 1               + matches_prefix        = []               + matches_storage_class = []               + matches_suffix        = []               + with_state            = (known after apply)             }         }     }  Plan: 2 to add, 0 to change, 0 to destroy.  Do you want to perform these actions?   Terraform will perform the actions described above.   Only 'yes' will be accepted to approve.    Enter a value: 